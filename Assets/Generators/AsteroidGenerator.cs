﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour
{
    public GameObject asteroidPrefab;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("GenAstro", 0.3f, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void GenAstro()
    {
        Instantiate(asteroidPrefab, new Vector3(-2.5f + 5 * Random.value, 6, 0), Quaternion.identity);
    }
}
