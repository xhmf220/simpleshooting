﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGen : MonoBehaviour
{
    public GameObject bossPrefab;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Wait");
        GenCube();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void GenCube()
    {
        Instantiate(bossPrefab, new Vector3(0, 8, 0), Quaternion.identity);
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(25f);
    }
}
