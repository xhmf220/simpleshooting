﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstGen : MonoBehaviour
{
    public GameObject burstPrefab;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("GenBurst", 30f,0.5f );
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void GenBurst()
    {
        Instantiate(burstPrefab, new Vector3(-2.5f + 5 * Random.value, 4f, 0), Quaternion.identity);
    }
}
