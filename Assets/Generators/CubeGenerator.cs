﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGenerator : MonoBehaviour
{
    public GameObject cubePrefab;
    // Start is called before the first frame update
    void Start()
    {
        
        InvokeRepeating("GenCube", 10f, Random.Range(2f, 3.0f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void GenCube()
    {
        Instantiate(cubePrefab, new Vector3(-2.5f + 5 * Random.value, 6, 0), Quaternion.identity);
    }
    


}
