﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AsteroidController : MonoBehaviour
{

    public int hp = 3;
    float fallSpeed;
    float rotSpeed;
    public GameObject particle;
    public GameObject particleEx;

    // Start is called before the first frame update
    void Start()
    {
        this.fallSpeed = 0.01f + 0.1f * Random.Range(0.3f,0.8f);
        this.rotSpeed = 5f + 3f * Random.value;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, -fallSpeed, 0, Space.World);
        transform.Rotate(0, 0, rotSpeed);

        if (transform.position.y < -10f)
        {
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
       
        if (coll.tag == "missile")
        {
            GameObject.Find("Canvas").GetComponent<ScoreController>().AddScore();
            hp--;
            Destroy(coll.gameObject);
            Instantiate(particle, transform.position, transform.rotation);

            
            if (hp == 0)
            {
                Instantiate(particleEx, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            
            

        }



    }
}
