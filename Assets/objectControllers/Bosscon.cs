﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bosscon : MonoBehaviour
{
     int hp =120;
    public GameObject particle;
    public GameObject particleEx;


    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(0, 0, 180);
        Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        StartCoroutine("Wait");
        rigid.bodyType = RigidbodyType2D.Kinematic;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "missile")
        {
            GameObject.Find("Canvas").GetComponent<ScoreController>().AddScore();
            hp--;
            Destroy(coll.gameObject);
            Instantiate(particle, transform.position, transform.rotation);


            if (hp == 0)
            {
                Instantiate(particleEx, transform.position, transform.rotation);
                Destroy(gameObject);
                StartCoroutine("Wait");
                SceneManager.LoadScene("ClearScene");
            }



        }



    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3.0f);
    }
}
