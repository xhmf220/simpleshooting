﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstCon : MonoBehaviour
{
    float fallSpeed;
    // Start is called before the first frame update
    void Start()
    {
        this.fallSpeed = 0.01f + 0.1f * 0.7f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, -fallSpeed, 0, Space.World);

        if (transform.position.y < -10f)
        {
            Destroy(gameObject);
        }

    }
    
}
