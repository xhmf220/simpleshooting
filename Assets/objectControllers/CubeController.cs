﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CubeController : MonoBehaviour
{
    public int hp = 7;
    public GameObject particle;
    public GameObject particleEx;

     GameObject objTarget;
    public float cubeSpeed = 1.5f;
    public Vector2 prev;
    void Start()
    {
        prev = transform.position;
        objTarget = GameObject.Find("playerShip");
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    void Move()
    {
        Vector2 Cube = objTarget.transform.position;
        float x = Cube.x;
        float y = Cube.y;

        Vector2 direction = new Vector2(x - transform.position.x, y - transform.position.y).normalized;
        GetComponent<Rigidbody2D>().velocity = (direction * cubeSpeed);

        //本体の向きを調整
        Vector2 Position = transform.position;
        Vector2 diff = Position - prev;
        if (diff.magnitude > 0.01)
        {
            float angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        prev = Position;
    }
    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "missile")
        {
            GameObject.Find("Canvas").GetComponent<ScoreController>().AddScore();
            hp--;
            Destroy(coll.gameObject);
            Instantiate(particle, transform.position, transform.rotation);


            if (hp == 0)
            {
                Instantiate(particleEx, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }
}
