﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerShipController : MonoBehaviour
{
    int hp = 5;

    public float moveSpeed = 0.1f;
    //減速
    public float decreaseSpeed = 0.05f;
    //発射レート
    public float fireRate =10f;

    bool firing = false;


    public GameObject missilePrefab;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-1 * moveSpeed, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(moveSpeed, 0, 0);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, moveSpeed, 0);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, -1 * moveSpeed, 0);
        }
        //減速モード
        if (Input.GetKey(KeyCode.C))
        {
            moveSpeed = decreaseSpeed;

        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            moveSpeed = 0.1f;
        }

        //ミサイル発射
        if (Input.GetKeyDown(KeyCode.Space))
        {
            firing = true;
            StartCoroutine("MissileFiring");

        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            firing = false;
        }

        
    }
    //弾連射コルーチン
    IEnumerator MissileFiring()
    {
        while (firing==true)
        {

            Instantiate(missilePrefab, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1f/fireRate);
        }
       
    }
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "enemy" || coll.tag == "burst")
        {
            hp--;
            GameObject director = GameObject.Find("gameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();
            
        }
        if(coll.tag == "boss")
        {
            SceneManager.LoadScene("GameOver");
        }
        if (hp == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
